function calculate_rectangle_area(length, width) {
  if (typeof length !== "number" || typeof width !== "number") {
    throw new Error("One or both of the arguments is not a number. Please provide two numbers.");
  }
  if (length <= 0 || width <= 0) {
    throw new Error("One or both of the arguments is less than or equal to 0. Please provide two positive numbers.");
  }
  return length * width;
}

module.exports = calculate_rectangle_area;
