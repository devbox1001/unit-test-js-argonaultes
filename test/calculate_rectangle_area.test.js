const calculate_rectangle_area = require("../src/calculate_rectangle_area");

test("calculate_rectangle_area should return the correct area for number arguments", () => {
  expect(calculate_rectangle_area(10, 5)).toBe(50);
  expect(calculate_rectangle_area(7, 3)).toBe(21);
  expect(calculate_rectangle_area(2, 8)).toBe(16);
});

test("calculate_rectangle_area should return the correct area for float arguments", () => {
  expect(calculate_rectangle_area(10.5, 5.5)).toBe(57.75);
  expect(calculate_rectangle_area(7.5, 3.5)).toBe(26.25);
  expect(calculate_rectangle_area(2.5, 8.5)).toBe(21.25);
});

test("calculate_rectangle_area should throw an error for non-number arguments", () => {
  expect(() => calculate_rectangle_area("10", 5)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(10, "5")).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area("10", "5")).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
});

test("calculate_rectangle_area should throw an error for arguments equal to 0", () => {
  expect(() => calculate_rectangle_area(0, 5)).toThrowError(
    "One or both of the arguments is less than or equal to 0. Please provide two positive numbers."
  );
  expect(() => calculate_rectangle_area(10, 0)).toThrowError(
    "One or both of the arguments is less than or equal to 0. Please provide two positive numbers."
  );
  expect(() => calculate_rectangle_area(0, 0)).toThrowError(
    "One or both of the arguments is less than or equal to 0. Please provide two positive numbers."
  );
});

test("calculate_rectangle_area should throw an error for arguments less than 0", () => {
  expect(() => calculate_rectangle_area(-1, 5)).toThrowError(
    "One or both of the arguments is less than or equal to 0. Please provide two positive numbers."
  );
  expect(() => calculate_rectangle_area(10, -1)).toThrowError(
    "One or both of the arguments is less than or equal to 0. Please provide two positive numbers."
  );
  expect(() => calculate_rectangle_area(-1, -1)).toThrowError(
    "One or both of the arguments is less than or equal to 0. Please provide two positive numbers."
  );
});

test("calculate_rectangle_area should throw an error for null or boolean arguments", () => {
  expect(() => calculate_rectangle_area(null, 5)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(10, null)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(null, null)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(true, 5)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(10, true)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(true, true)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(false, 5)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(10, false)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(false, false)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
});

test("calculate_rectangle_area should throw an error for object arguments", () => {
  expect(() => calculate_rectangle_area({}, 5)).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area(10, {})).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
  expect(() => calculate_rectangle_area({}, {})).toThrowError(
    "One or both of the arguments is not a number. Please provide two numbers."
  );
});
